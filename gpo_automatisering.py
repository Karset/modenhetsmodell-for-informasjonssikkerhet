#!/usr/bin/python
# coding=utf-8

# For XML
from lxml import etree

# For SQL
import mysql.connector

# Gjøre om liste med tupler til liste
import itertools 


# For XML
# Path til XML-ark for RSoP
tree = etree.parse("/home/karset/Documents/bachelor/rsop.xml")

# Path i XML-ark til innstillingene for computer results og user results
# Policy-nummer må endres slik at det matcher riktig policy-nummer i RSoP
path = ["/*[name()='Rsop']/*[name()='ComputerResults']/*[name()='ExtensionData']/*[name()='Extension']/*[name()='q19:Policy']","/*[name()='Rsop']/*[name()='UserResults']/*[name()='ExtensionData']/*[name()='Extension']/*[name()='q8:Policy']"]


# For SQL
# Kobler til databasen
mydb = mysql.connector.connect(
        host="localhost",
        user="newuser",
        passwd="-",
        database="gpo_settings",
        charset='utf8',
        use_unicode=True
        )

# De ulike tabellene i databasen
table = ["CRes", "URes"]

# Ny liste for Kategori, Subkategori, Innstilling, og AS IS radene i CRes
CRes_kat = []
CRes_sub = []
CRes_innst = []
CRes_asis = []

# Ny liste for Kategori, Subkategori, Innstilling, og AS IS radene i URes
URes_kat = []
URes_sub = []
URes_innst = []
URes_asis = []

# Variabel for å iterere gjennom path
it = 0

# Itererer gjennom tabellene i databasen
while it < len(table):

    # Felles lister for Kategori, Subkategori, Innstilling, og AS IS
    arr_kat = []
    arr_sub = []
    arr_innst = []
    arr_asis = []

    # Definerer SQL cursor
    mycursor = mydb.cursor()

    # Itererer gjennom XML-filen
    for name in tree.xpath(path[it]):

        # Variabel som sier om innstillingen er en subinnstilling
        sub = "no"

        # Henter barna til root
        kids = name.getchildren()

        # Itererer gjennom barne nodene (innstillingene)
        for kid in kids:

            # Henter ut kategori navnet til innstillingen
            if "Category" in kid.tag:
                kat = kid.text
                arr_kat.append(kat)

            # Henter ut navnet til innstillingen
            if "Name" in kid.tag:
                innst = kid.text
                arr_innst.append(innst)
                arr_sub.append(sub)

            # Henter ut tilstanden (asis) til innstillingen
            if "State" in kid.tag:
                asis = kid.text
                arr_asis.append(asis)

            # Sjekker om innstillingen har underinnstillinger
            # Hvis så, hent ut navn og tilstand
            # Ser etter DropDownList underinnstillinger
            if "DropDownList" in kid.tag:
                arr_kat.append(kat)
                sub = innst
                arr_sub.append(sub)
                dropdown = kid.getchildren()
                for drop in dropdown:
                    if "Name" in drop.tag:
                        innst = drop.text
                        arr_innst.append(innst)
                    if "State" in drop.tag:
                        if drop.text == "NotConfigured":
                            arr_asis.append("NotConfigured")
                    values = drop.getchildren()
                    for value in values:
                        if "Name" in value.tag:
                            asis = value.text
                            arr_asis.append(asis)
                innst = sub

            # Ser etter CheckBox under innstillinger
            if "CheckBox" in kid.tag:
                arr_kat.append(kat)
                sub = innst
                arr_sub.append(sub)
                checkbox = kid.getchildren()
                for check in checkbox:
                    if "Name" in check.tag:
                        innst = check.text
                        arr_innst.append(innst)
                    if "State" in check.tag:
                        asis = check.text
                        arr_asis.append(asis)
                innst = sub


            # Ser etter Numeric underinnstillinger
            if "Numeric" in kid.tag:
                arr_kat.append(kat)
                sub = innst
                arr_sub.append(sub)
                numbers = kid.getchildren()
                for number in numbers:
                    if "Name" in number.tag:
                        innst = number.text
                        arr_innst.append(innst)
                    if "Value" in number.tag:
                        asis = number.text
                        arr_asis.append(asis)
                innst = sub
    
    
    # Legger funnene fra XML inn i tabell spesifikke lister
    # For CRes
    if table[it] == "CRes":
        for n in arr_kat:
            CRes_kat.append(n)
        for n in arr_sub:
            CRes_sub.append(n)
        for n in arr_innst:
            CRes_innst.append(n)
        for n in arr_asis:
            CRes_asis.append(n)
    
    # For URes
    elif table[it] == "URes":
        for n in arr_kat:
            URes_kat.append(n)
        for n in arr_sub:
            URes_sub.append(n)
        for n in arr_innst:
            URes_innst.append(n)
        for n in arr_asis:
            URes_asis.append(n)


    # Tabellspesifikke endringer
    # For CRes
    if table[it] == "CRes":
        k = 0

        # Hvis en innstilling har None verdi, gi den en "-" verdi
        while k < len(CRes_innst):
            if CRes_innst[k] == None:
                CRes_innst[k] = "-"
            k += 1

        # Tabellspesifikk sql statement
        sql = "SELECT Kategori, Subkategori, Innstilling, TO_BE, Poeng FROM CRes WHERE Kategori = %s AND Subkategori = %s AND Innstilling = %s"
        
        # Fyller felles variabler med tabellspesifikk informasjon
        gpo_kat = CRes_kat
        gpo_sub = CRes_sub
        gpo_innst = CRes_innst
        gpo_asis = CRes_asis
        gpo_totscore = 0
        gpo_not_eq = []
        gpo_name = "Computer Results"
        gpo_maxscore = 0


    # For URes
    elif table[it] == "URes":
        j = 0

        # Hvis en innstilling har "\n      " verdi, gi den en "-" verdi
        while j < len(URes_innst):
            if URes_innst[j] == "\n        ":
                URes_innst[j] = "-"
            j+=1

        # Tabellspesifikk sql statement
        sql = "SELECT Kategori, Subkategori, Innstilling, TO_BE, Poeng FROM URes WHERE Kategori = %s AND Subkategori = %s AND Innstilling = %s"
        
        # Fyller felles variabler med tabellspesifikk informasjon
        gpo_kat = URes_kat
        gpo_sub = URes_sub
        gpo_innst = URes_innst
        gpo_asis = URes_asis
        gpo_totscore = 0
        gpo_not_eq = []
        gpo_name = "User Results"
        gpo_maxscore = 0


    # Variabel for å iterere gjennom innstillingene
    o = 0

    # Itererer gjennom innstillingene for å sammenlikne AS IS med TO BE
    while o < len(gpo_kat):


        # Henter ut raden i databasen som matcher Kategori, Subkategori, og Innstilling fra XML funnene
        arg = (gpo_kat[o], gpo_sub[o], gpo_innst[o])
        mycursor.execute(sql, arg)
        results = mycursor.fetchall()

        # Gjør om resultatene fra liste med tupler til liste
        results = list(itertools.chain(*results))

	# Så lenge en match blir funnet
	if len(results) > 0:
		gpo_maxscore += results[4]

        	# Sjekker om TO BE kolonnen skal ha spesialbehandling
        	if str(results[3])[0] == '*':

        	    	# Hvis 'l' skal AS IS matche en av de listede tilstandene i TO BE
			if results[3][1] == 'l':
			        gpo_isscore = 0
			        tobe_value = results[3][3:len(results[3])]
		 	        liste = tobe_value.split(",")
		 	        for n in liste:
		 	           if gpo_asis[o] == n and gpo_isscore == 0:
		        	        gpo_totscore += results[4]
		        	        gpo_isscore = 1
		        
		        	# Hvis AS IS ikke matchet med TO BE og mulig poeng ikke er lik 0, skal raden legges
		        	#inn i "not equal" listen
		        	if results[4] != 0 and gpo_isscore == 0:
		            		gpo_not_eq.append(gpo_kat[o])
		            		gpo_not_eq.append(gpo_sub[o])
		            		gpo_not_eq.append(gpo_innst[o])
		            		gpo_not_eq.append(gpo_asis[o])
		            		gpo_not_eq.append(tobe_value)

		    	# Hvis 'b' skal AS IS være større eller lik TO BE
		    	elif str(results[3])[1] == 'b':
		        	tobe_value = results[3].split(" ")
		        	tobe_value = int(tobe_value[2])
		       		asis_value = gpo_asis[o].split(" ")
		       		asis_value = int(asis_value[0])
		       		if asis_value >= tobe_value:
		       	   		gpo_totscore += results[4]
		        
		        	# Hvis AS IS ikke mathcet med TO BE og mulig poeng ikke er lik 0, skal raden legges
		        	#inn i "not equal" listen
		        	else:
		            		if results[4] != 0:
		                		gpo_not_eq.append(gpo_kat[o])
		                		gpo_not_eq.append(gpo_sub[o])
		                		gpo_not_eq.append(gpo_innst[o])
		                		gpo_not_eq.apppend(asis_value)
		                		gpo_not_eq.append(str(">=" + tobe_value))
		                           
		    	# Hvis 's' skal AS IS være mindre eller lik med TO BE
		    	elif str(results[3])[1] == 's':
		        	tobe_value = results[3].split(" ")
		        	tobe_value = int(tobe_value[2])
		        	asis_value = gpo_asis[o].split(" ")
		        	asis_value = int(asis_value[0])
		        	if asis_value <= tobe_value:
		        	    gpo_totscore += results[4]
		        
		        	# Hvis AS IS ikke matchet med TO BE og mulig poeng ikke er lik 0, skal raden legges
		        	#inn i "not equal" listen
		        	else:
		        	    if results[4] != 0:
		        	        gpo_not_eq.append(gpo_kat[o])
		        	        gpo_not_eq.append(gpo_sub[o])
		        	        gpo_not_eq.append(gpo_innst[o])
		        	        gpo_not_eq.append(asis_value)
		        	        gpo_not_eq.append(str("<=" + tobe_value))

		    	# Hvis 't' skal AS IS være i intervallet spesifisert i TO BE
		    	elif str(results[3])[1] == 't':
		    	    tobe_value = results[3].split(" ")
		    	    start = int(tobe_value[1])
		    	    stop = int(tobe_value[3])
		    	    asis_value = gpo_asis[o].split(" ")
		    	    asis_value = int(asis_value[0])
		    	    if asis_value >= start and asis_value <= stop:
		    	        gpo_totscore += results[4]
		        
		    	    # Hvis AS IS ikke matchet med TO BE og mulig poeng ikke er lik 0, skal legges
		    	    #inn i "not equal" listen
		    	    else:
		    	        if results[4] != 0:
		    	            gpo_not_eq.append(gpo_kat[o])
		    	            gpo_not_eq.append(gpo_sub[o])
		    	            gpo_not_eq.append(gpo_innst[o])
		    	            gpo_not_eq.append(asis_value)
		    	            gpo_not_eq.append(str("Større enn" + start + "mindre enn" + stop))

		    	# Hvis 'n' skal AS IS ikke være lik TO BE
		    	elif str(results[3])[1] == 'n':
		    	    tobe_value = results[3].split(" ")
		    	    tobe_value = tobe_value[2]
		    	    if gpo_asis[o] != tobe_value:
		    	        gpo_totscore += results[4]
		        
		    	    # Hvis AS IS ikke matchet med TO BE og mulig poeng ikke er lik 0, skal raden legges
		    	    #inn i "not equal" listen
		    	    else:
		    	        if results[4] != 0:
		    	            gpo_not_eq.append(gpo_kat[o])
		    	            gpo_not_eq.append(gpo_sub[o])
		    	            gpo_not_eq.append(gpo_innst[o])
		    	            gpo_not_eq.append(asis)
		    	            gpo_not_eq.append(str("Ikke lik" + tobe_value))

		# Hvis det ikke kreves spesialbehandling skal AS IS matche TO BE
		elif gpo_asis[o] == results[3]:
		    	gpo_totscore += results[4]

		# Hvis AS IS ikke matchet med TO BE og mulig poeng ikke er lik 0, skal raden legges
		#inn i "not equal" listen
		elif results[4] != 0:
		    	gpo_not_eq.append(gpo_kat[o])
		    	gpo_not_eq.append(gpo_sub[o])
		   	gpo_not_eq.append(gpo_innst[o])
		   	gpo_not_eq.append(gpo_asis[o])
	 		gpo_not_eq.append(results[3])

        # Øker til neste innstilling
        o+=1


    # Presentering av resultatet
    # Variabel for å iterere gjennom "not equal" listen
    i = 0

    # Printer ut tabellnavn, oppnådd score, og innstillinger som ikke matcher beste praksis
    print "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    print "                              ", gpo_name
    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    print "\nTotal oppnådd score: ", gpo_totscore, " av ", gpo_maxscore
    if len(gpo_not_eq) > 0:
        print "\nInnstillinger som ikke samsvarte med beste praksis:"
        print "____________________________________________________________________________\n"
        while i < len(gpo_not_eq):
            print gpo_not_eq[i]
            if gpo_not_eq[i+1] != "no":
              print "   ->", gpo_not_eq[i+1]
            print "      ->", gpo_not_eq[i+2]
            print "\nNåværende tilstand:", gpo_not_eq[i+3]
            print "Burde være:        ", gpo_not_eq[i+4]
            print "____________________________________________________________________________\n"
            i+=5

    # Øker til neste tabell
    it +=1


