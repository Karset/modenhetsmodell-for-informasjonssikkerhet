# Modenhetsmodell for Informasjonssikkerhet
Dette repositoriet består av komponenter relatert til modenhetsmodellen Modenhetsmodell for Informasjonssikkerhet. For å fortså hvordan modellen skal brukes, anbefales det å lese igjennom modellens dokumentasjon.

Modenhetsmodellen er utviklet som en del av en bacheloroppgave i IT-drift og Informasjonssikkerhet.
